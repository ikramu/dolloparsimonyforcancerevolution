
Inferring tumor evolution using Dollo Parsimony in R
===========================================
This is the R code for reconstructing tumor evolution (phylogenetic) trees using somatic mutations calls from multiple samples (for the same patient). The program expects a data frame with columns representing samples and rows representing genes having mutations. The matrix is in binary format where `1` represents the presence and `0` the absence of a mutation. 

Pre-requisite tools for running the script
------------------------
Install the following tools before running the script.

1. **PYHLIP**: PHYLIP is a free package of programs for inferring phylogenies. It is available from http://evolution.genetics.washington.edu/phylip/getme.html PHYLIP contains implementation for different phylogenetic algorithms including Dollo parsimony (named *dollop* in the library)
2. **Rphylip**:  This is a wrapper for PHYLIP library in R. Available from CRAN
3. **Ape** and **Phangorn**: These are R phylogenetic libraries available from CRAN. 

After installing the above tools
-------------------------------------------
In the the file `performDolloBootstraping.R`, change the `path` variable to the directory containing PHYLIP executable *dollop* (currently `/usr/local/bin/`) in the following line

    bValuesDP = my.boot.phylo(treeDRooted, zeroOneMatrix, FUN=function(xx) { 
		    Rdollop(xx, method=method, quite=TRUE, 
		    path='/usr/local/bin/') 
    }, B = bsIterations, trees=T, rooted=T)

The code expects a binary matrix (named `vaf` in the code) where `vaf[i,j]` is 1 if *ith* sample has *jth* mutation. Since the `getVafData()` call in `dolloRunAllSitesBootstrapForAllPatients()` is custom-made for our data, you may use your own function for creating `vaf`. 

Keep in mind that the program expects that `ccf.table` (defined as `ccf.table = ccfDataList[[i]]` in `dolloRunAllSamples.R`) contains both the mutation calls and purity information for all the samples in a patient. If you do not have the purity information, you can set them to `1` (meaning `100%` purity) for each sample. 

For any queries, create an issue ticket and I will try to reply ASAP.

Cheers
Ikram 