source('dolloPerformBootstraping.R')
source('ccfUtilities.R')
library(Rphylip)

#' Performs Dollo parsimony analysis along with bootstrapping for 
#' the patients in the cohort. 
#' 
#' @param ccfDataList: List of mutation calls. Each member of the list is a 
#' mutation matrix for one patient, e.g., ccfDataList[[1]] is for mutation matrix
# from Mutect analysis for 1st patient, ccfDataList[[20]] is for 20th patient and so on
#' @param vaf.cutoff: Cutoff used for presence/absence of a mutation. For our analysis, we
#' used a cutoff of 0.05 as given in Method section of the paper
#' @param outDirPref: Main directory where, for each patient, one directory will be made 
#' to write the results
#' @param bsIterations: Number of bootstrap iterations, default is 1000 
#' @param useCountAsThreshold: Set to TRUE if we want to perform thresholding on 
#' the basis of total number of reads for each mutation, false otherwise
#' @param scaleVafByPurity: set to TRUE when we want to scale the allele frequencies in a sample 
#' by its purity, false otherwise
dolloRunAllSitesBootstrapForAllPatients = function(ccfDataList, vaf.cutoff, outDirPref, bsIterations=1000, 
                                                   useCountAsThreshold = FALSE, scaleVafByPurity = TRUE) {
	for(i in c(1:5,7:11,13:20)) {
	  ccf.table = ccfDataList[[i]]
	  
	  if(scaleVafByPurity) {
      if(i != 20) {
        ccf.table = scaleAllSamplesByASCATPurity(pID = i, dat = ccf.table)
      }
	  }
	  
	  outDir = paste0(outDirPref, '/', i)
	  
	  unlink(outDir, recursive = TRUE)
	  dir.create(outDir, showWarnings = FALSE)
	  
	  countGt2 = NULL
	  if(useCountAsThreshold) {
	    countGt2 = getRefAltAlleleCountThreshold(ccf.table)
	  }
	  
    # vaf is S x M binary matrix with S samples and M mutations 
	  vaf = getVafData(ccf.table=ccf.table, isBinary=TRUE, countGt2 = countGt2, vaf.cutoff = vaf.cutoff)
	  #vaf = getVafData(ccf.table=ccf.table, isBinary=TRUE, countGt2 = countGt2, vaf.cutoff = 0)
	  
		bsTrees = performDolloBootstraping(vaf, bsIterations, outDir, i)

		print(paste0('Done for patient ', i))
		print('###########|||||||||||||############||||||||||||##########')
	}
}

#' Scale variant allele frequencies using ASCAT-based tumor purity estimates 
#'
#' @param pID The ID of the patient
#' @param dat The data containint the purity estimates, in our case, we
#' have purity and mutation information in the same matrix so we use the 
#' same 'ccf.table'
scaleAllSamplesByASCATPurity = function(pID, dat) {
  # I am taking 3, rather than 1, just to remove any chance of 
  # odd behavior due to vector confusion in a single row data.frame
  purityValues = dat[1:3, grep('purity', names(dat))]
  vaf = dat[, grep('tumor_', names(dat))]
  names(purityValues) = gsub('purity_', '', names(purityValues))
  names(vaf) = gsub('tumor_f_', '', names(vaf))
  
  vafNames = names(vaf)
  purityNames = names(purityValues)
  
  for(i in 1:length(vafNames)) {
    if(vafNames[i] %in% purityNames) {
      nvalues = dat[, paste0('tumor_f_',vafNames[i])] / purityValues[1, vafNames[i]]
    } else {
      pv = getMissingSampleASCATValue(pID, vafNames[i])
      nvalues = dat[, paste0('tumor_f_',vafNames[i])] / pv
    }
    nvalues[which(nvalues > 1)] = 1
    dat[, paste0('tumor_f_',vafNames[i])] = nvalues
  }
  
  return(dat)
}

#' For four samples where ASCAT analysis failed, we use the purity
#' estimates reported by pathology
#' @param pID The ID of the patient
#' @param  sampleName name of the sample 
getMissingSampleASCATValue = function(pID, sampleName) {
  if((pID == 1) & (sampleName == 'Lung1.R1')) {
    return (0.85)
  } else if((pID == 10) & (sampleName == 'ALN1')) {
    return (0.80)
  } else if((pID == 15) & (sampleName == 'ALN')) {
    return (0.80)
  } else if((pID == 20) & (sampleName == 'Ovary5.R2')) {
    return (0.50)
  }
}


#' Scale variant allele frequencies of a samples using its tumor purity estimates 
#' 
scaleAllSamplesByPurity = function(ccf.table) {
  tumorPurity = read.xlsx('data/Tumor_cell_percentage_mitosis grade.xls', 
                          sheetIndex = 1, header = T, stringsAsFactors = F)
  vaf = ccf.table[, grep('tumor_f', names(ccf.table))]
  
  ids = strsplit(names(vaf), split = '\\.')
  
  # The third token is sample ID
  for(i in 1:length(ids)) {
    sid = ids[[i]][3]
    colName = names(vaf)[i]
    print(paste0('ID is ', sid))
    ccf.table[, colName] = scaleVAFByPurity(ccf.table[, colName], sid, tumorPurity)
  }
  
  return(ccf.table)
}

#' Scale variant allele frequencies using tumor purity estimates 
#
#' Given the tumor purity estimates, scale VAF values in each sample by
#' corresponding purity value
#' @param combData The big CCF table containing all columns for VAF, copy number etc
#' @param geneSet vector of DRIVER genes (either COSMIC or Yates2015)
#' @return sub-matrix of ccf.table
scaleVAFByPurity = function(oldVaf, sid, tumorPurity) {
  s1BlockId = sid
  
  # for some blocks we have 'B' in the end, check that
  if(substr(s1BlockId, start = nchar(s1BlockId), stop = nchar(s1BlockId)) == 'B')
    s1BlockId = substr(s1BlockId, start = 1, stop = nchar(s1BlockId)-1)
  
  
  s1Purity = tumorPurity[which(tumorPurity$Block.ID == s1BlockId), 'T.cells.']
  
  newVaf = oldVaf / s1Purity
  
  # if some VAF is greater than 1 after scaling, set it to 1
  newVaf = sapply(newVaf, function(x) min(x,1))
  
  return(newVaf)
}

